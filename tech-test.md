Technical Test for Platform Engineers
=====================================

Instructions
------------

The purpose of this assignment is to assess your problem solving
approach and your ability to produce idempotent system management tools.

You are required to:

-   Produce some code to automate the build of a server

-   Provide clear instructions for running your code

-   State clearly any assumptions made (either in code comments or as a
    > separate README file)

-   Use source control

If you have any issues with the assignment, please ensure that you
inform the test coordinator immediately. You are expected to work on
this task on your own, without help or advice from others. If you need
clarification on any aspect of the assignment, please seek help from
your test coordinator. Upon completion of your test please e-mail your
submission to your test coordinator.

The test does not include a set time limit, but the time it takes you to
implement your solution may be taken into account during assessment.

Assignment
----------

### Summary

You will be given an IP address and an SSH private key for the
**ubuntu** user to allow you to log into a server which is already
configured. You are required to analyse the configuration of the server,
establish its flaws, identify the appropriate fixes and apply them using
your chosen toolset. The solution should work against existing servers
such as the one provided, and unprovisioned servers (clean OS installs).
The user story below explains in more detail what is required.

### Background

The University of Leodis runs a farm of Apache web servers behind a load
balancer, which acts as the frontend to their entire online estate,
including the Undergraduate Course prospectus, Student VLE and Staff
Email. The Web team are continuously frustrated by the time taken by the
System Administration team to make simple Apache configuration updates;
and after years of undocumented tweaks and hacks, the System
Administrators are reluctant to make any sizeable changes to it.

### User Story

  ------------------------------------------------------------------------------------------------------------------------------------------
  #### Make rapid, repeatable changes to the web tier

  **In order to** allow the Web developers to implement additional Apache configuration changes in a timely manner

  **As a** platform engineer

  **I need to** have a mechanism to easily apply the changes to the servers in any environment

  **Acceptance Criteria**

  -   Flaws with the existing server setup are highlighted

  -   Tool produced to restore the servers into a satisfactory state (i.e. fix the issues), and handle future Apache configuration changes

  ------------------------------------------------------------------------------------------------------------------------------------------
  ------------------------------------------------------------------------------------------------------------------------------------------

**Further implementation notes**

-   You may use any configuration management frameworks/tools as you see
    > fit

-   The code must be idempotent

-   The code must check for and report errors

-   The code should meet the requirements and be able to support future
    > extension

-   Consider how you might test that your changes have not affected
    > functionality

**Assumptions you can make**

The backend in the proxypass statements have been modified for the
purpose of this tech test. Modifying the backend is out of scope for
this test.
