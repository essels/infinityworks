------------
Observations
------------

The current server has some problems that open it up to be hacked if left unchanged.
These include:

- apache in non-stndard location, this makes it harder to update apache using the default package manager of the OS
- apache running as root, if apache is compromised, the attacker gains full admin priviledges on the box
- server root is pointing to apache config and executable
- all folders are 777
- Nodejs application is also running as root

------
Remedy
------

To fix these problems we have a tool developed using Ansible that will ensure the state and config of apache is managed via code.
No manual changes should be made to the box and all updates to the apache config can be pushed from this repo.


### Pre-requistes


We assume the user has a working python/pip setup on his/her laptop alongwith a working `virtualenvwrapper` setup in bash.
You can setup your new python environment to work with ansible as follows:

	# Create a new virtualenv
	mkvirtualenv ansible

	# Install python packages
	pip install -r requirements.txt


### Ansible Config

Next we will need to configure the path for the `pem` key to access the server. Edit the following file with the path `plays/hosts`
and replace `/absolute/path/to/user.pem` appropriately.

	ansible_ssh_private_key_file=/absolute/path/to/user.pem

Once this is done you should get a response back from the server using ansible by running this in the `plays` folder:

	$ cd plays 

	$ ansible -m ping leodis-prod         
	leodis-prod | SUCCESS => {
	    "changed": false, 
	    "ping": "pong"
	}


### Running the ansible playbook

To configure the server simply run the following while in the `plays` folder:
	
	ansible-playbook infinity-leodis.yml


## How it all works

Here is an overview of the folder structure we have in place, with a brief explanation of the notable files.

	.
	├── ansible.cfg 			# Ansible config, controls default behaviour of our ansible run
	├── base-setup				# Base-setup ROLE
	│   ├── README.md
	│   ├── defaults
	│   │   └── main.yml
	│   ├── files
	│   ├── handlers
	│   │   └── main.yml
	│   ├── meta
	│   │   └── main.yml
	│   ├── tasks
	│   │   └── main.yml
	│   ├── templates
	│   ├── tests
	│   │   ├── inventory
	│   │   └── test.yml
	│   └── vars
	│       └── main.yml
	├── create-ec2.retry 		# ansible generated file
	├── create-ec2.yml 			# Create a NEW AWS server and configure apache (Nodejs TODO)
	├── hosts					# Hosts ansible will run on
	├── infinity-leodis.retry 	# ansible generated file
	├── infinity-leodis.yml 	# Actions to perform on the LEODIS server
	└── leodis-apache 			# Leodis-apache ROLE
	    ├── README.md
	    ├── defaults
	    │   └── main.yml
	    ├── files
	    ├── handlers
	    │   └── main.yml
	    ├── meta
	    │   └── main.yml
	    ├── tasks
	    │   └── main.yml
	    ├── templates
	    │   ├── 000-default.j2 		# Default apache site config
	    │   ├── index.html.j2 		# Default index page shown
	    │   ├── leodis-apache.j2 	# Global apache config
	    │   └── leodis-vhost.j2 	# Leodis vhost config
	    ├── tests
	    │   ├── inventory
	    │   └── test.yml
	    └── vars
	        └── main.yml


Please consult the  `infinity-leodis.yml` and related `tasks/main.yml` files in the two ROLES to review the sequence of steps run on the box. 
The advantage of ansible is that we can use plain english for each step and allows us to self-document the provisioning code.

To help me in developing the roles used to configure apache, I created a playbook `create-ec2.yml`. This playbook will create a instance in AWS and configure it to the end state from scratch. This means the same code works on an existing server as shown and also a brand new server.

