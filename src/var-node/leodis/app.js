var express = require('express');
var hbs = require('express-handlebars');
var path = require('path');
var app = express();

app.engine('hbs', hbs({
  extname:'hbs',
  defaultLayout:'main.hbs',
  layoutsDir: path.join(__dirname, 'views/layouts/')
}));

app.set('views', path.join(__dirname, 'views/'));
app.set('view engine', 'hbs');

app.get('/', function(req, res) {
  res.render('index');
});

app.get('/courses', function(req, res) {
  res.render('courses');
});

app.get('/courses/french', function(req, res) {
  res.render('course', { course: 'French' });
});

app.get('/courses/computing', function(req, res) {
  res.render('course', { course: 'Computing' });
});

app.get('/undergraduate', function(req, res) {
  res.render('undergraduate');
});

app.get('/mail', function(req, res) {
  res.render('mail');
});

app.get('/vle', function(req, res) {
  res.render('vle');
});

var server = app.listen(1337);
